//
//  ViewController.swift
//  imagechanger
//
//  Created by Click Labs on 1/12/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var imageNumber: UITextField!
    @IBOutlet weak var imagePlane: UIImageView!
    @IBAction func changeButton(sender: AnyObject) {
        self.view.endEditing(true)		
        var number = imageNumber.text
        switch number {
        case "1":
             var image = UIImage(named:"image1.png")
             imagePlane.image = image
        case "2":
            
            var image = UIImage(named:"image2.png")
            imagePlane.image = image
        case "3":
            
            var image = UIImage(named:"image3.jpg")
            imagePlane.image = image
        case "4":
            
            var image = UIImage(named:"image4.png")
            imagePlane.image = image
        case "5":
            
            var image = UIImage(named:"image5.jpg")
            imagePlane.image = image
        case "6":
            
            var image = UIImage(named:"image6.png")
            imagePlane.image = image
        case "7":
            
            var image = UIImage(named:"image7.jpg")
            
            imagePlane.image = image
            
        case "8":
            
            var image = UIImage(named:"image8.jpg")
            imagePlane.image = image
        case "9":
            
            var image = UIImage(named:"image9.png")
            imagePlane.image = image
        case "10":
            
            var image = UIImage(named:"image10.png")
            imagePlane.image = image
        default:
            var image = UIImage(named:"image1.png")
        imagePlane.image = image
            
        }
        
    }
   
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
                // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

